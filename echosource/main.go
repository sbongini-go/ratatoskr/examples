package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/customhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/randomwaithandler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"gitlab.com/sbongini-go/ratatoskr/core/source/echosource"
)

func main() {
	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.INFO),
	)

	coreObj := core.NewCore(
		core.WithServiceName("example-echosource"),
		core.WithServiceVersion("v0.1.0"),
		core.WithServiceNamespace("ns-exampe"),
		core.WithServiceAttribute("environment", "example"),
		core.WithLogger(logger),
		core.WithSource("echo-source", echosource.New(
			echosource.WithPhrases("aaaa", "bbbb", "error", "cccc"),
			echosource.WithDelay(100*time.Millisecond),
			echosource.WithInterval(200*time.Millisecond),
			echosource.WithNext("random-wait"),
		)),
		core.WithHandler("random-wait",
			randomwaithandler.New(
				"end",
				100*time.Millisecond,
				200*time.Millisecond,
			),
		),
		core.WithHandler("end", customhandler.New(func(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
			if string(event.Data) == "error" {
				return event, fmt.Errorf("happy error")
			} else {
				fmt.Println("end => ", string(event.Data))
			}
			return event, nil
		})),
	)

	coreObj.Start()
}
