module gitlab.com/sbongini-go/ratatoskr/examples/kafka-mirror-2

go 1.20

require (
	github.com/rs/zerolog v1.26.1
	gitlab.alm.poste.it/go/ilog/ilog v0.1.2
	gitlab.alm.poste.it/go/ilog/zerolog v0.1.3
	gitlab.com/sbongini-go/ratatoskr/core v0.0.0-20220121182116-285bbd5aeffd
	gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler v0.0.0-20211222094235-bab3db7dc637
	gitlab.com/sbongini-go/ratatoskr/source/kafkasource v0.0.0-20211222093442-8fa30a6d373e
	go.opentelemetry.io/otel/exporters/jaeger v1.3.0
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.3.0
	go.opentelemetry.io/otel/exporters/zipkin v1.3.0
)
