# Esempio - Kafka Mirror

Tramite il file `config.yaml` viene creata un istanza di Ratatroskr che legge da due topic `input-a` e `input-b` e scrive sul topic `output`. In caso di errore prova ascrivere sul topic `discard`, se anche questa scrittura dovesse andare in errore verrà restituito un errore configurato all'interno del `config.yaml`.

![Esempio Kafka Mirror](./docs/kafka-mirror2.png)

## Esecuzione

Per eseguire l'esempio:

1. Avviamo il docker-compose presente nella cartella `../../env` con il seguente comando 

   ```sh
   docker-compose up
   ```

2. Avviamo il client Kaf per interagire con il cluster Kafka

   ```sh
   # Configuriamo l'accesso al custer Kafka
   kaf config add-cluster local -b localhost:9092
   # Indichiamo a Kaf che vogliamo usare il cluster di nome "local"
   kaf config use-cluster local
   ```

2. Creiamo i topic per far funzionare la demo

   ```sh
   kaf topic create "input-a" --partitions 3 --replicas 1
   kaf topic create "input-b" --partitions 3 --replicas 1
   kaf topic create "output" --partitions 3 --replicas 1
   kaf topic create "discard" --partitions 3 --replicas 1
   ```

3. Avviare i seguenti 3 terminali:
   
   Terminale 1:
   
   ```sh
   # Avviamo l'applicazione
   go run .
   ```
   
   Terminale 2:
   
   ```sh
   # Mettiamoci in ascolto sul topic di output per vedere i messaggi che arrivano
   kaf consume -f "output"
   # Comando alternativo con Kafkacat
   # kafkacat -b localhost:9092 -t "output"
   ```
   
   Terminale 3:
   
   ```sh
   # Produciamo messaggi su uno dei topic di input
   kaf produce "input-a"
   # Comando alternativo con Kafkacat
   # kafkacat -b localhost:9092 -t "input-a" -P
   ```

## Monitoraggio

Sarà possibile monitorare il servizio tramite:

* Tracciatura, andando su:
  * Jaeger: http://localhost:16686/
  * Zipkin: http://127.0.0.1:9411/
* Metriche, andando su:
  * l'exporter di Ratatoskr: http://127.0.0.1:2112/metrics
