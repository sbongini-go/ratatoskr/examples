package main

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core/factory"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/branchhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/echohandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/randomwaithandler"
	"gitlab.com/sbongini-go/ratatoskr/core/source/echosource"
	"gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler"
	"gitlab.com/sbongini-go/ratatoskr/source/kafkasource"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/exporters/zipkin"
)

func main() {
	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.DEBUG),
	)

	// Istanzio gli exporter per OpenTelemetry
	jaegerExporter, _ := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://localhost:14268/api/traces")))
	stdoutExporter, _ := stdouttrace.New()
	zipkinExporter, _ := zipkin.New("http://localhost:9411/api/v2/spans")

	var coreFactory = factory.New(
		factory.WithSourceFunc("echo", echosource.NewFromInterface),
		factory.WithSourceFunc("kafka", kafkasource.NewFromInterface),
		factory.WithHandlerFunc("echo", echohandler.NewFromInterface),
		factory.WithHandlerFunc("branch", branchhandler.NewFromInterface),
		factory.WithHandlerFunc("random-wait", randomwaithandler.NewFromInterface),
		factory.WithHandlerFunc("kafka", kafkahandler.NewFromInterface),
	)

	coreObj, err := coreFactory.CreateCoreFromFile("config.yaml")
	if nil != err {
		panic(fmt.Errorf("error create core from yaml: %w", err))
	}

	coreObj.SetLogger(logger)

	coreObj.AddSpanExporter(jaegerExporter)
	coreObj.AddSpanExporter(stdoutExporter)
	coreObj.AddSpanExporter(zipkinExporter)

	coreObj.Start()
}
