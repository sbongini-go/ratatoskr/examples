module gitlab.com/sbongini-go/ratatoskr/examples/echosource-fromyaml

go 1.20

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/prometheus/common v0.32.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/rs/zerolog v1.26.1
	gitlab.alm.poste.it/go/ilog/ilog v0.1.2
	gitlab.alm.poste.it/go/ilog/zerolog v0.1.3
	gitlab.com/sbongini-go/ratatoskr/core v0.0.0-20211222090201-09d9e7320f83
	go.opentelemetry.io/otel/exporters/jaeger v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
)
