# Esempio - EchoSource

Tramite il file `config.yaml` viene creata un istanza di Ratatroskr  che genera eventi, viene applicato un ritardo casuale alla propagazione del evento e in caso il messaggio generato contenga la stringa "error" viene rilanciato un errore, altrimenti viene stampato l'evento sullo standard output.

![Esempio EchoSource](../echosource/docs/echosource-example.png)

## Esecuzione

Avviamo l'applicazione con il seguente comando: `go run .`.

Si potranno vedere sullo standard output l'esito di quello che sta succedendo, ovvero gli eventi stampati e gli errori generati.

```
end =>  aaaa
end =>  bbbb
{"level":"error","err":"happy error","_name":"echo-source","_type":"source","time":"2022-05-30T12:37:39+02:00","message":"output"}
end =>  cccc
end =>  aaaa
end =>  bbbb
{"level":"error","_type":"source","err":"happy error","_name":"echo-source","time":"2022-05-30T12:37:40+02:00","message":"output"}
end =>  cccc
end =>  aaaa
end =>  bbbb
{"level":"error","_name":"echo-source","_type":"source","err":"happy error","time":"2022-05-30T12:37:40+02:00","message":"output"}
end =>  cccc
```



