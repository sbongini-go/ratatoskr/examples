package main

import (
	"context"
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/factory"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/branchhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/customhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/echohandler"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/randomwaithandler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"gitlab.com/sbongini-go/ratatoskr/core/source/echosource"
)

func main() {
	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.INFO),
	)

	var coreFactory = factory.New(
		factory.WithSourceFunc("echo", echosource.NewFromInterface),
		factory.WithHandlerFunc("echo", echohandler.NewFromInterface),
		factory.WithHandlerFunc("branch", branchhandler.NewFromInterface),
		factory.WithHandlerFunc("random-wait", randomwaithandler.NewFromInterface),
	)

	coreObj, err := coreFactory.CreateCoreFromFile("config.yaml")
	if nil != err {
		panic(fmt.Errorf("error create core from yaml: %w", err))
	}

	coreObj.SetLogger(logger)
	coreObj.AddHandler("end", customhandler.New(func(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
		if string(event.Data) == "error" {
			return event, fmt.Errorf("happy error")
		} else {
			fmt.Println("end => ", string(event.Data))
		}
		return event, nil
	}))

	coreObj.Start()
}
