# Examples

In questa cartella si trovano alcuni esempi di utilizzo di *Ratatroskr*:

> TODO Lista esempi

| Nome Esempio                                 | Descrizione                                                  | Source       | Handler                        |
| -------------------------------------------- | ------------------------------------------------------------ | ------------ | ------------------------------ |
| [kafka-mirror](./kafka-mirror/README.md)     | Legge da 2 topics in input e scrive su un topic in output.   | kafka-source | kafka-handler                  |
| [kafka-mirror-2](./kafka-mirror-2/README.md) | Legge da 2 topics in input e scrive su un topic in output, in caso di errore prova a scrivere su un topic di errore, se l'errore persiste rilancia un errore. | kafka-source | kafka-handler, fallback, error |
|                                              |                                                              |              |                                |

## TODO 
Di segutio alcuni esempi da implementare:

